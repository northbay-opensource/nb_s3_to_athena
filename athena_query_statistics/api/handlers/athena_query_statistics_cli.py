__author__ = 'haris.aziz'

import argparse
from api.services.athena_query_statistics_processor import *
from nb_utils.config_loader import *
import logging


# Generate Athena DDL from S3 data
# Accepts as input the following:
#   * configfile - specifies the configuration
#
def main():
    ap = argparse.ArgumentParser(description='Generate Athena DDL from S3 data')
    ap.add_argument("-c", "--config", help="Configuration for the conversion")
    ap.add_argument("--debug", help="display debug messages")
    args = ap.parse_args()

    logging.debug("configfile", args.config)

    process_entity(args.config)


def process_entity(config_file):
    """

    :param config_file:
    :return:
    """

    logging.info(f"Loading Config file")
    config = ConfigLoader.get_configuration(config_file)
    process_athena_query_stats = ProcessingAthenaQueryStatistics(config['output']['athena']['statistics'])
    try:
        # Start the process to Generate Athena DDL from S3 data
        logging.info(f'Start process to run athena queries and gather statistics')
        process_athena_query_stats.execute_athena_queries()
        logging.info(f'queries ran successfully.')
    except Exception as err:
        logging.error(f'Error while running queries on athena: {err}')


if __name__ == "__main__":
    main()
