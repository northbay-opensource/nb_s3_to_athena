from nb_utils.aws_utils import AWSGlueUtils, AWSS3Utils, AWSAthenaUtils
import logging


class ProcessingAthenaQueryStatistics:
    """
    Class to Run Queries on athena and gather statistics
    """

    def __init__(self, config: dict):
        """
        Constructor
        :param config:
        """

        self.__config = config
        self.__glue_utils = AWSGlueUtils()
        self.__s3_utils = AWSS3Utils()
        self.__athena_utils = AWSAthenaUtils()

    @staticmethod
    def add_stats_in_file(database_name, query, table_name, run_id, engine_ex_time, data_scanned, row_count, file_name):
        """
        writing stats in the file

        :param database_name:
        :param query:
        :param table_name:
        :param run_id:
        :param engine_ex_time:
        :param data_scanned:
        :param row_count:
        :param total_ex_time:
        :param queue_time:
        :param service_time:
        :param file_name:
        :return:
        """
        f = open(file_name, "a")
        stats = f"{database_name},{query},{table_name},{run_id},{engine_ex_time},{data_scanned},{row_count}\n"
        f.write(stats)
        f.close()

    def execute_athena_queries(self):
        """
         running query on athena and writing stats in the file

         :return:
         """
        db_name = self.__config['database_name']
        file_name = self.__config['file_name']
        workgroup = self.__config['workgroup']

        athena_query_output_location = self.__config['athena_query_output_location']
        partition_column = self.__config['partition_column']
        partition_value_to_query = self.__config['partition_value_to_query']

        try:

            if not self.__glue_utils.check_glue_database(db_name):
                raise Exception(f"No database found with name : {db_name}")
            logging.info(f"Database found in glue catalog")
            # fetching all tables in the database
            table_list = self.__glue_utils.get_tables_in_database(db_name)
            # Adding headers in stats file
            self.add_stats_in_file('databaseName', 'query', 'table', 'runID', 'engineExecutionTimeInSec',
                                   'dataScannedInMegaBytes', 'rowCount', file_name)
            error_list = {}
            for table in table_list:
                try:
                    table_name = table['Name']
                    query = "SELECT * from " + table_name
                    print(query)
                    execution_id, row_count = self.__athena_utils.run_athena_query(query, db_name, workgroup,
                                                                                   athena_query_output_location)
                    print(execution_id)
                    print(row_count)
                    stats = self.__athena_utils.get_athena_query_execution(execution_id)
                    athena_stats = stats['QueryExecution']['Statistics']
                    print(athena_stats)
                    self.add_stats_in_file(db_name, query, table_name, 'None',
                                           athena_stats['EngineExecutionTimeInMillis'] / 1000,
                                           athena_stats['DataScannedInBytes'] / 1024 / 1024, row_count, file_name)

                    partition_query = f"SELECT * FROM {table_name} WHERE {partition_column}='{partition_value_to_query}'"
                    print(partition_query)
                    execution_id, row_count = self.__athena_utils.run_athena_query(partition_query, db_name, workgroup,
                                                                                   athena_query_output_location)
                    print(execution_id)
                    print(row_count)
                    stats = self.__athena_utils.get_athena_query_execution(execution_id)
                    athena_stats = stats['QueryExecution']['Statistics']
                    self.add_stats_in_file(db_name, partition_query, table_name, partition_value_to_query,
                                           athena_stats['EngineExecutionTimeInMillis'] / 1000,
                                           athena_stats['DataScannedInBytes'] / 1024 / 1024, row_count, file_name)

                except Exception as err:
                    error_list[table_name] = err
            print(error_list)
            logging.info(f"Query error list: {error_list}")
        except Exception as err:
            logging.info(f"{err}")
            raise Exception(err)


