from nb_utils.aws_utils import AWSGlueUtils, AWSS3Utils
import logging


class AthenaDDLGenerationProcessor:
    """
    Class to Generate Athena database and tables
    """

    def __init__(self, config: dict):
        """
        Constructor
        :param config:
        """
        self.__config = config
        self.__glue_utils = AWSGlueUtils()
        self.__s3_utils = AWSS3Utils()

    @staticmethod
    def get_athena_column_json(header_column_list):
        """
        convert athena column name and column type dictionary to json object

        :param header_column_list:
        :return:
        """
        json_array = []
        for column_name in header_column_list:
            obj = {'Name': column_name,
                   'Type': header_column_list[column_name]}
            json_array.append(obj)
        return json_array

    @staticmethod
    def get_table_and_partitions(key_list, prefix):
        """
        get table names and partitions from s3 object list

        :param key_list:
        :param prefix:
        :return:
        """

        table_dic = {}
        temp_dic = {}
        partition_dic = {}

        for obj in key_list:
            key = obj['Key']
            split_list = key.split(prefix)[1].split('/')

            if split_list[len(split_list) - 1] == '':
                continue

            table_name = split_list[0]
            partition_number = split_list[1].split('=')[1]

            if table_name in temp_dic.keys():
                if temp_dic[table_name] != partition_number:
                    p_list = partition_dic[table_name]
                    p_list.append(partition_number)
                    partition_dic[table_name] = p_list
                if temp_dic[table_name] < partition_number:
                    table_dic[table_name] = key
                    temp_dic[table_name] = partition_number
            else:
                table_dic[table_name] = key
                temp_dic[table_name] = partition_number
                p_list = [partition_number]
                partition_dic[table_name] = p_list

        del temp_dic

        return table_dic, partition_dic

    def create_glue_tables(self, db_name, table_properties):
        """
        Generate table ddl from s3 csv files and create in glue database

        :param db_name:
        :param table_properties:
        :return:
        """

        bucket = table_properties['bucket']
        prefix = table_properties['prefix']
        partition_column = table_properties['partition_column']
        delimiter = table_properties['delimiter']

        # getting list of objects from s3
        object_list = self.__s3_utils.get_s3_objects(bucket, prefix)

        # Getting table and partitions from s3 objects prefixes
        tables, partitions = self.get_table_and_partitions(object_list, prefix)

        # iterating over tables list to generate ddl
        for table in tables:
            try:
                # getting table headers and data types by sampling data through pandas data frame
                column_info = self.__s3_utils.get_header_and_types(bucket, tables[table], delimiter)
            except Exception as err:
                raise Exception(f"unable to get headers for table {tables[table]}: {err}")

            # get athena column json from headers and data types
            athena_col_list = self.get_athena_column_json(column_info)

            # preparing table location
            location = f"s3://{bucket}/{prefix}{table}/"
            try:
                self.__glue_utils.create_glue_table(db_name, table, athena_col_list, partition_column, location,
                                                    delimiter)
            except Exception as err:
                logging.error(f'Error in creating table: {err}')
                raise Exception(err)

            # loading partitions for the table
            for partition in partitions[table]:
                self.__glue_utils.create_partition(db_name, table, athena_col_list, partition,
                                                   location + partition_column + '=' + partition + '/', delimiter)

    def create_glue_database(self, database):
        """
        Create Glue database if does not exist through Glue API
        :param database:
        :return:
        """

        if not self.__glue_utils.check_glue_database(database):
            self.__glue_utils.create_glue_database(database)

    def generate_ddl(self):
        """
        runs the process to create glue database and glue tables
        :return:
        """

        self.create_glue_database(self.__config['database_name'])
        self.create_glue_tables(self.__config['database_name'], self.__config['table_properties'])
