__author__ = 'haris.aziz'

import argparse
from api.services.athena_ddl_generation_processor import *
from nb_utils.config_loader import *
import logging


# Generate Athena DDL from S3 data
# Accepts as input the following:
#   * configfile - specifies the configuration
#
def main():
    ap = argparse.ArgumentParser(description='Generate Athena DDL from S3 data')
    ap.add_argument("-c", "--config", help="Configuration for the conversion")
    ap.add_argument("--debug", help="display debug messages")
    args = ap.parse_args()

    logging.debug("configfile", args.config)

    process_athena_ddl_generation(args.config)


def process_athena_ddl_generation(config_file):
    """

    :param config_file:
    :return:
    """

    logging.info(f"Loading Config file")
    config = ConfigLoader.get_configuration(config_file)
    athena_ddl_generator = AthenaDDLGenerationProcessor(config['input']['athena'])
    try:
        # Start the process to Generate Athena DDL from S3 data
        logging.info(f'Start the Generate Athena DDL from S3 data process')
        athena_ddl_generator.generate_ddl()
        logging.info(f'DDL generated successfully.')
    except Exception as err:
        logging.error(f'Error while generating DDL: {err}')


if __name__ == "__main__":
    main()
